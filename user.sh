useradd -m -s /bin/bash -g wheel boykov

cd /home/boykov
curl https://gitlab.com/fz.boykov/ssh-key/raw/master/id-rsa.pub > id-rsa.pub
mkdir .ssh
cp id-rsa.pub .ssh/authorized_keys
chown -R boykov:root .ssh
chmod -R 0700 .ssh
